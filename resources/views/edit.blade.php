@extends('layouts.blade', ['title'=>'Película '])

@section('body')

<center>
	<h1>
      Editar {{$pelicula->titulo}}
	</h1>

	<h2>
<a href="{{route('peliculas.index')}}">Lista de películas</a>
    </h2>
</center>

<form style="text-align: center;" action="{{route('peliculas.update',$pelicula)}}"" method="POST" enctype="multipart/form-data">
    	{{csrf_field()}}
    	{{method_field('PUT')}}
    	<p>
    		<label for="titulo">Título</label>
    		<input type="text" name="titulo" id="titulo" placeholder="Título"
    		value="{{old('titulo',$pelicula->titulo)}}">

    	</p>
    	<p>
    		<label for="ano">Año</label>
    		<input type="number" name="ano" id="ano" placeholder="Año"
    		value="{{old('ano',$pelicula->ano)}}">

    	</p>
      
      

      
      <center>  
    	<p>
    		<label for="poster">Poster</label>
    		<input type="file" name="poster" id="poster">

    	</p>
      <p>
      
      
      <label for="productora_id">Productora</label>
      
      <select name="productora_id">
        <option selected disabled>Seleccionar productora</option>
        @foreach ($productoras as $productora)
        <option value="{{$productora->id}}"
           @if (old('productora_id', $pelicula->productora_id)==$productora->id)
           selected
           @endif
           > {{$productora->nombre}}

          </option>
          @endforeach
        
         </select>


        </p>


        <p>
         <label for="pgeneros">Genero</label> 
          <select id="generos" name="generos[]" multiple>
        
        @foreach ($generos as $genero)
        <option value="{{$genero->id}}"
           @if (in_array($genero->id,old ('generos',$pelicula->generos->pluck('id')->toArray())))
           selected
           @endif
           > {{$genero->nombre}}

          </option>
          @endforeach
        
         </select> 

        </p>


      </center>

    	<p>
    		<button>Enviar</button>
    	</p>

    </form>

    <center>

    <form action="{{route('peliculas.destroy',$pelicula)}}"" method="POST">
   {{csrf_field()}}
   {{method_field('DELETE')}}


        <p>
         	<input type="submit" value="Eliminar">
    	</p>
    	

    </form>

    </center>
    @stop