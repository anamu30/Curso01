<!DOCTYPE html>
<html>
<head>
	

	<meta charset="utf-8">
    
    <title>{{$title}}</title>
    
	@section('estilos')
	<link rel="stylesheet" type="text/css" href="{{ mix('/css/app.css') }}">
	@show
    
</head>
<body>
	
   
   @yield('body')

   

   <script type="{{mix('/js/app.js')}}"></script>

</body>
</html>