@extends('layouts.blade', ['title'=>'Película ' . $pelicula->titulo])

@section('body')
<center>
  <h2>Título: {{$pelicula->titulo}}</h2>
  <h3>Año: {{$pelicula->ano}}</h3>

@if ($pelicula->productora)
<p>
  Productora: {{$pelicula->productora->nombre}}
</p>
@endif

@if ($pelicula->generos && $pelicula->generos->count()>0)
   <p>
     Géneros: {{$pelicula->generos->implode('nombre',',')}}
   </p>
@endif

  @if ($pelicula->poster)
     <img src="{{url($pelicula->poster)}}"/>
  @endif
  @auth
  <p>
  	<a href="{{route('peliculas.edit',$pelicula)}}">Editar película</a>
  </p>
  @endauth

   <p>
  	<a href="{{route('peliculas.index',$pelicula)}}">Lista de películas</a>
  </p>
</center>
