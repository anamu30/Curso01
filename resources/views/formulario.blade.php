@extends('layouts.blade', ['title' => 'Formulario']) 

@section('body')

   <h1 style="text-align: center;">Introducir película</h1>

   <center>

   <p><a href="{{route('peliculas.index')}}">Lista de películas</a></p>
     </center>
  
     @if ($errors->any())
           <div class="alert alert-danger">
           	<ul>
           		@foreach ($errors->all() as $error)
           		<li>{{$error}}</li>
                @endforeach

           	</ul>
           	

           </div>
     @endif


    <form style="text-align: center;" action="{{route('peliculas.store')}}"" method="POST" enctype="multipart/form-data">
    	{{csrf_field()}}
    	<p>
    		<label for="titulo">Título</label>
    		<input type="text" name="titulo" id="titulo" placeholder="Título"
    		value="{{old('titulo')}}">

    	</p>
    	<p>
    		<label for="ano">Año</label>
    		<input type="number" name="ano" id="ano" placeholder="Año"
    		value="{{old('ano')}}">

    	</p>
      
      

      
      <center>  
    	<p>
    		<label for="poster">Poster</label>
    		<input type="file" name="poster" id="poster">

    	</p>
      <p>
      
      
      <label for="productora_id">Productora</label>
      

      <select name="productora_id">
        <option selected disabled>Seleccionar productora</option>
        @foreach ($productoras as $productora)
        <option value="{{$productora->id}}"
           @if (old('productora_id')==$productora->id)
           selected
           @endif
           > {{$productora->nombre}}

          </option>
          @endforeach
        
         </select>


        </p>


        <p>
         <label for="pgeneros">Genero</label> 
          <select id="generos" name="generos[]" multiple>
        
        @foreach ($generos as $genero)
        <option value="{{$genero->id}}"
           @if (old('generos') && in_array($genero->id, old('generos')))
           selected
           @endif
           > {{$genero->nombre}}

          </option>
          @endforeach
        
         </select> 




        </p>


      </center>




    	<p>
    		<button>Enviar</button>
    	</p>


    </form>

@stop