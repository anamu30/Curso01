
@extends('layouts.app', ['title'=>'Peliculas'])

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title>Películas</title>
	<h1 style="text-align: center;">Películas</h1>
    <h2>
    
    <center>
    <a href="{{route('principal')}}">Página principal</a>
     </center>

      </h2>
</head>
<body>
    <center>
    <table style="align-content: center;" border="1">
    	<tr>
    		<td>
    			<p>Título</p>
    		</td>
    		<td>
    			<p>Año</p>
    		</td>
            @auth
    		<td>
    			<p>Editar</p>
    		</td>
            @endauth

    	</tr>
    	@foreach ($pelicula->all() as $pelicula)
    	<tbody>  	
    	<tr>
    		<td>
    			
    			<a href="{{route('peliculas.show',$pelicula)}}">{{$pelicula->titulo}}</a>

    		</td>
    	    		
    		<td>
    			
    			{{$pelicula->ano}}

    		</td>
            @auth
    		<td>
                
    			<a href="{{route('peliculas.edit', $pelicula)}}">Editar</a>
                
    		</td>
            @endauth
    	</tr>
    	</tbody>
    	@endforeach
        @auth
    	<a href="{{route('peliculas.create')}}">Crear película</a>	
        @endauth
    </table>

    </center>
    @stop

</body>
</html>