<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelicula;
use App\Productor; 
use App\Genero;
use View;

class PeliculaController extends Controller
{
    
    public function __construct(){
        

                 /*
        
        $this->middleware(function($request, $next){
        if (!$request->wantsJson()){
            if (Auth::guest()){

                abort('403',"A dónde vas??");
            }
        }
        return $next($request);
    })->except('index','show');
    */

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pelicula = Pelicula::all();

        if ($request->wantsJson()){
            return $peliculas;
        }
        
        return View::make('index', compact('pelicula'));



        //return Pelicula::all(['titulo', 'ano']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productoras = Productor::all();
        $generos = Genero::all();
        return View::make('formulario', compact('productoras', 'generos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'titulo'=>'required|string|max:200',
          'ano' => 'required|integer|min:1900|max:2020',
          
          'poster' => 'image',

          'productora_id' => 'required|exists:productoras,id',

          'generos' => 'required|array','generos.*' => 'exists:generos,id'
        ]);
        
        /*
        $pelicula =new Pelicula;
        $pelicula->titulo=$request->get('titulo');
        $pelicula->ano=$request->get('ano');
        $pelicula->identificador=$request->get('identificador');
        */

        $properties = $request->only('titulo', 'ano');
        $properties['identificador'] = Pelicula::generateUniqueSlug($request->get('titulo'));
        if ($request->has('poster')) {
            $archivo=$request->file('poster');
            $nombrearchivo=$archivo->getClientOriginalName();
            $archivo->move(public_path() . '/uploads/', $nombrearchivo);
            $properties['poster'] = '/uploads/' . $nombrearchivo;

        }

        $pelicula = Pelicula::create($properties);

        $pelicula->generos()->sync($request->get('generos'));

        if ($request->wantsJson()){
            return $pelicula;
        }

        return redirect(route('peliculas.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Pelicula $pelicula, Request $request)
    {
        if ($request->wantsJson()){
            return $pelicula;
        }
        return View::make('detail',compact('pelicula'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Pelicula $pelicula)
    {

        $productoras = Productor::all();
        $generos = Genero::all();
        return View::make('edit',compact('pelicula','productoras','generos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pelicula $pelicula)
    {
        $request->validate([
          'titulo'=>'required|string|max:200',
          'ano' => 'required|integer|min:1900|max:2020',
          
          'poster' => 'image',

          'productora_id' => 'required|exists:productoras,id',

          'generos' => 'required|array','generos.*' => 'exists:generos,id'
        ]);
        

        $properties = $request->only('titulo', 'ano');
        
        if ($request->has('poster')) {
            $archivo=$request->file('poster');
            $nombrearchivo=$archivo->getClientOriginalName();
            $archivo->move(public_path() . '/uploads/', $nombrearchivo);
            $properties['poster'] = '/uploads/' . $nombrearchivo;

        }

        $pelicula->fill($properties);

        $pelicula->generos()->sync($request->get('generos'));

        $pelicula->save();

        if ($request->wantsJson()){
            return $pelicula;
        }
        
        return redirect(route('peliculas.show', $pelicula));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pelicula $pelicula, Request $request)
    {
        $pelicula->delete();
        if ($request->wantsJson()){
            return [' '];
        }
        return redirect(route('peliculas.index'));
    }
}
