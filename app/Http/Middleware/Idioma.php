<?php

namespace App\Http\Middleware;

use Closure;

class Idioma
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->route('locale');

         App::setLocale($locale);

        return $next($request);
    }
}
