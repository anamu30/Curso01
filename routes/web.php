<?php

use \App\Pelicula;
use App\Productor; 
use App\Genero; 

Route::get('/test3', function () {
  $pelicula = Pelicula::first();
  

  $pelicula->productora()->dissociate();
  $pelicula->save();
  return $pelicula;

});

 
 
Route::get('/test2', function () { 

/*    

  $productor = new Productor;     
  $productor->nombre = "Sony";     
  $productor->save(); 
 
    $productor = new Productor;     
    $productor->nombre = "Disney";     
    $productor->save(); 
 
    $productor = new Productor;     
    $productor->nombre = "Fox";     
    $productor->save(); 
 
    $productor = new Productor; 
    $productor->nombre = "Warner Bros";  
    $productor->save(); 
 
    $productor = new Productor;     
    $productor->nombre = "MGM";     
    $productor->save(); 
 
    $genero = new Genero;     
    $genero->identificador = 'accion';     
    $genero->nombre = 'Acción';     
    $genero->save(); 
 
    $genero = new Genero;     
    $genero->identificador = 'comedia';     
    $genero->nombre = 'Comedia';     
    $genero->save(); 
 
    $genero = new Genero;     
    $genero->identificador = 'drama';     
    $genero->nombre = 'Drama';     
    $genero->save(); 
 
    $genero = new Genero;     
    $genero->identificador = 'terror';     
    $genero->nombre = 'Terror';     
    $genero->save(); 

    */

    return Pelicula::select('peliculas.*')->join('productoras','productoras.id','=','peliculas.productora_id')
    ->where('productoras.nombre', 'Sony')->get();




  }); 

Route::get('/pagprueba', function () {

return View::make('principal');


  //crear registro
	// $pelicula=new Pelicula;
 //  $pelicula->identificador='rambo';
 //  $pelicula->titulo='Rambo';
 //  $pelicula->ano=1982;
 //  $pelicula->save();

  
//cambiar registro
  //$pelicula->titulo = 'Acorralado';
  //$pelicula->save();

//eliminar
  //$pelicula->delete();
  //Pelicula::destroy([2,3,4,5]);

 /* 
Pelicula::where('identificador', '=', 'rambo')->update([
       'titulo' => 'Acorralado',
       'identificador' => 'acorralado'

    ]);
    */

 //return Pelicula::take(2)->get();
  

  //$pelicula = Pelicula::find(3);
 // return $pelicula->titulo;

  //return Pelicula::rockySaga()->pluck('titulo');


    /*associar
   
  $pelicula = Pelicula::first();
  $productora = Productor::first();

  $pelicula->productora()->associate($productora);
  $pelicula->save();
  return $pelicula;
  */

  /* sincronizar tabla genero_pelicula
  $genero = Genero::first();

  $peliculas = [3,4,5,6,7];

  $genero->peliculas()->sync($peliculas);
  $genero->save();
  return $genero->peliculas;

  */




})->name('principal');


Route::get('/test4', function () {
  
  $productor = Productor::first();
  $pelicula = Pelicula::where('identificador', 'acorralado')->first();
  $pelicula2 = Pelicula::where('identificador', 'rocky')->first();
  $pelicula3 = Pelicula::where('identificador', 'creed')->first();
$productor->peliculas()->saveMany([$pelicula,$pelicula2,$pelicula3]);
$productor->save();

   });


Route::get('/test', function () {
  

  /*
 $pelicula = new Pelicula;
 $pelicula->identificador = "rambo";
 $pelicula->titulo = "Rambo";
 $pelicula->ano = 1982;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rambo-ii";
 $pelicula->titulo = "Rambo II";
 $pelicula->ano = 1985;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rambo-iii";
 $pelicula->titulo = "Rambo III";
 $pelicula->ano = 1988;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "john-rambo";
 $pelicula->titulo = "John Rambo";
 $pelicula->ano = 2008;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rocky";
 $pelicula->titulo = "Rocky";
 $pelicula->ano = 1976;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rocky-ii";
 $pelicula->titulo = "Rocky II";
 $pelicula->ano = 1979;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rocky-iii";
 $pelicula->titulo = "Rocky III";
 $pelicula->ano = 1982;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rocky-iv";
 $pelicula->titulo = "Rocky IV";
 $pelicula->ano = 1986;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rocky-v";
 $pelicula->titulo = "Rocky V";
 $pelicula->ano = 1990;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "rocky-balboa";
 $pelicula->titulo = "Rocky Balboa";
 $pelicula->ano = 2006;
 $pelicula->save();
 $pelicula = new Pelicula;
 $pelicula->identificador = "creed";
 $pelicula->titulo = "Creed";
 $pelicula->ano = 2016;
 $pelicula->save();
 */

//$pelicula = Pelicula::find(3);
  //return Pelicula::where('ano', '=', 1982)->pluck('titulo');
  //return Pelicula::where('ano', '=', 1982)->where('titulo', 'Acorralado')->get();
$productor = Productor::first();
return $productor->peliculas()->where('ano', '<', 2000)->get();

});


Route::get('/segunda', function () {
 return Pelicula::whereNested(function($query)
 {
 $query->where('ano', '>', 2000);
 $query->where('identificador', 'LIKE', '%rambo%');
 })->orWhere(function ($query)
 {
 $query->where('ano', '<', 2000);
 $query->where('identificador', 'LIKE', '%rocky%');
 })->get();
})->name('segunda');


Route::get('/peliculas', function () {

 



})->name('peliculas');

Route::get('/creartoken',function(){
  $user=Auth::user();
  $token=$user->createToken('hola')->accessToken;
  return $token;

})->middleware('auth');




Route::resource('peliculas', 'PeliculaController');



Route::get('/formulario', function (){
  return View::make('formulario');
})->name('formulario');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/pruebusu', function(){
  if (Auth::check()) {
    return "autenticado";
  } else {
    return "no autenticado";
  }


})->middleware('auth')->name('home');

/*
Route::group(function(){
  Route::get('/formulario', function (){
  return View::make('formulario');
})->name('email');

})->middleware('auth')->prefix('pruebas')
*/
